var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var swig = require('swig');

var indexRouter = require('./routes/index');
var institucionalRouter = require('./routes/institucional');
var beneficiosRouter = require('./routes/beneficios');
var productosRouter = require('./routes/productos');
var planesRouter = require('./routes/planes');
var email = require('./routes/email');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('views', path.join(__dirname, 'views'));
// Swig will cache templates for you, but you can disable
// that and use Express's caching instead, if you like:
app.set('view cache', false);
// To disable Swig's cache, do the following:
swig.setDefaults({ cache: false });
// NOTE: You should always cache templates in a production environment.
// Don't leave both of these to `false` in production!

app.use('/', indexRouter);
app.use('/institucional', institucionalRouter);
app.use('/beneficios', beneficiosRouter);
app.use('/productos', productosRouter);
app.use('/planes', planesRouter);
app.use('/email', email);

module.exports = app;
