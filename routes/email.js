var express = require('express');
var router = express.Router();
var Email = require('../controllers/email');

/* GET home page. */
router.post('/', Email.sendEmail);

module.exports = router;
