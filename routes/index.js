var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  console.log(req.query)
  if (req.query.fueEnviado == 'si' || req.query.fueEnviado == 'no')
    res.render('index', { title: 'Mi test', fueEnviado: req.query.fueEnviado});
  else
    res.render('index', { title: 'Mi test' });
});

module.exports = router;