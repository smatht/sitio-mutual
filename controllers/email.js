var nodemailer = require('nodemailer')

exports.sendEmail = function(req, res){
  let from = ''
  let to = 'amsic2014@hotmail.com'
  let cc = ['juankamendiburu@hotmail.com', 'sanexpeditoctes@hotmail.com']
  let bcc = ['estefi5587@gmail.com']
  let subject = ''
  let text = ''
  let nombre = ''
  let formaContacto = 'Email'
  let contacto = ''

  var transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    requireTLS: true,
    auth: {
        user: 'info.amsic@gmail.com',
        pass: 'amsic2020'
    }
  })

  if (req.body.emailInput)
    subject = 'Solicitud de ampliacion de información'
  else
    subject = 'Solicitud de afiliación desde sitio Amsic.'

  if (req.body.recipientName)
    nombre = req.body.recipientName
  else 
    nombre = `${req.body.nombreInput} ${req.body.apellidoInput}`
    
  if (req.body.recipientTel) {
    formaContacto = 'Telefono'
    contacto = req.body.recipientTel
  }
  else 
    contacto = req.body.emailInput

  text = `Nombre y apellido: ${nombre}
    ${formaContacto}: ${contacto}
    Mensaje: ${req.body.messageText}`
  from = req.body.emailInput

  var mailOptions = { from, to, cc, bcc, subject, text }

  transporter.sendMail(mailOptions, function(error, info){
    if (error){
        console.log(error);
        //res.send(500, error.message);
        res.redirect('/?fueEnviado=no')
    } else {
        console.log("Email sent");
        //res.status(200).jsonp(req.body);
        res.redirect('/?fueEnviado=si')
    }
})
}